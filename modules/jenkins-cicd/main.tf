provider "aws" {
  region = "us-east-1"
  alias  = "certificates"
}

# Specify the provider and access details
module "common" {
  source       = "git::https://bitbucket.org/anoopdhopte/common.git//modules?ref=1.2.2"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = var.ENVIRONMENT
  PROJECT      = var.PROJECT
}

module "networking" {
  source                  = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/networking?ref=2.1.3"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = [cidrsubnet(var.VPC_CIDR_BLOCK, 3, 0), cidrsubnet(var.VPC_CIDR_BLOCK, 3, 1)]
  APPLICATION_CIDR_BLOCKS = [cidrsubnet(var.VPC_CIDR_BLOCK, 3, var.AZ_COUNT), cidrsubnet(var.VPC_CIDR_BLOCK, 3, var.AZ_COUNT + 1)]
  DATA_CIDR_BLOCKS        = [cidrsubnet(var.VPC_CIDR_BLOCK, 3, var.AZ_COUNT + 2), cidrsubnet(var.VPC_CIDR_BLOCK, 3, var.AZ_COUNT + 3)]
  USE_NAT_GATEWAY         = var.USE_NAT_GATEWAY
  # FLOW_LOGS_ENABLED       = "true"
  # FLOW_LOGS_BUCKET_ARN    = module.audit_logs.s3_bucket_arn
}

module "encryption" {
  source  = "git::https://bitbucket.org/anoopdhopte/encryption.git//modules?ref=1.0.5"
  globals = module.common.globals
}

module "s3_logs" {
  source          = "git::https://bitbucket.org/anoopdhopte/s3.git//modules/logs?ref=1.1.1"
  globals         = module.common.globals
  EXPIRATION_DAYS = var.EXPIRATION_DAYS
}

module "dns_zone" {
  source                  = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/zone_public?ref=1.1.0"
  globals                 = module.common.globals
  DNS_ZONE_NAME           = var.DNS_ZONE_NAME
  CREATE_ZONE_CERTIFICATE = var.CREATE_ZONE_CERTIFICATE
}
