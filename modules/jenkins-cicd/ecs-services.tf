/*
Task describing Jenkins Master object
Use AWS Fargate as lunch type
*/
resource "aws_ecs_service" "jenkins_master" {
  name                               = "jenkins-master-service"
  cluster                            = module.ecs_cluster.cluster_arn
  task_definition                    = aws_ecs_task_definition.jenkins_master.arn
  desired_count                      = var.JENKINS_MASTER.INSTANCES
  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0
  launch_type                        = "FARGATE"
  platform_version                   = "1.4.0"

  network_configuration {
    security_groups = [aws_security_group.jenkins_master_sg.id]
    subnets         = module.networking.application_subnet_ids
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.jenkins_master_http_target_group.arn
    container_name   = "jenkins-master"
    container_port   = var.JENKINS_MASTER.INTERNAL_PORT
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.jenkins_master_http_internal_target_group.arn
    container_name   = "jenkins-master"
    container_port   = var.JENKINS_MASTER.INTERNAL_PORT
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.jenkins_master_jnlp_target_group.arn
    container_name   = "jenkins-master"
    container_port   = var.JENKINS_MASTER.JNLP_PORT
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.jenkins_master_service.arn
    container_name = "jenkins-master"
  }
}

resource "aws_ecs_service" "squid" {
  name             = "squid-service"
  cluster          = module.ecs_cluster.cluster_arn
  task_definition  = aws_ecs_task_definition.squid.arn
  desired_count    = var.SQUID.INSTANCES
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    security_groups = [aws_security_group.squid.id]
    subnets         = module.networking.application_subnet_ids
  }

}

/*Service definition for the anchore scanner*/

resource "aws_ecs_service" "anchore" {
  name             = "anchore"
  cluster          = module.ecs_cluster.cluster_arn
  task_definition  = aws_ecs_task_definition.anchore.arn
  desired_count    = var.ANCHORE.INSTANCES
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    security_groups = [aws_security_group.anchore.id]
    subnets         = module.networking.application_subnet_ids
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.anchore_service.arn
    container_name = "engine-api"
  }
}

resource "aws_ecs_service" "sonarqube" {
  name             = "SonarQube"
  cluster          = module.ecs_cluster.cluster_arn
  task_definition  = aws_ecs_task_definition.sonarqube.arn
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    security_groups = [aws_security_group.sonarqube.id]
    subnets         = module.networking.application_subnet_ids
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.sonar_target_group_external.arn
    container_name   = "sonarqube"
    container_port   = var.SONARQUBE.INTERNAL_PORT
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.sonarqube_service.arn
    container_name = "sonarqube"
  }
}

resource "aws_ecs_service" "nist_sync" {
  name             = "nist-sync"
  cluster          = module.ecs_cluster.cluster_arn
  task_definition  = aws_ecs_task_definition.nist_sync.arn
  desired_count    = var.NIST_SYNC.INSTANCES
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    security_groups = [aws_security_group.nist_mirror.id]
    subnets         = module.networking.application_subnet_ids
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.nist_service.arn
    container_name = "nist-sync"
  }
}

resource "aws_ecs_service" "grafana" {
  name             = "grafana"
  cluster          = module.ecs_cluster.cluster_arn
  task_definition  = aws_ecs_task_definition.grafana.arn
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  load_balancer {
    target_group_arn = aws_alb_target_group.grafana_http_target_group.arn
    container_name   = "grafana"
    container_port   = var.GRAFANA.INTERNAL_PORT
  }

  network_configuration {
    security_groups = [aws_security_group.grafana.id]
    subnets         = module.networking.application_subnet_ids
  }

  service_registries {
    registry_arn   = aws_service_discovery_service.grafana_service.arn
    container_name = "grafana"
  }
}
