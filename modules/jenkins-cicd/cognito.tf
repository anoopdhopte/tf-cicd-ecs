resource "aws_cognito_user_pool_domain" "cognito_domain_name" {
  domain          = "auth.jenkins.${var.DNS_ZONE_NAME}"
  certificate_arn = module.auth_certificate.certificate_arn
  user_pool_id    = module.cognito.id
}

module "cognito" {
  source                       = "git::https://bitbucket.org/anoopdhopte/cognito.git//modules/user_pool?ref=1.2.0"
  globals                      = module.common.globals
  NAME                         = "${var.ENVIRONMENT}-${var.PROJECT}"
  RESOURCE_SERVER_IDENTIFIER   = "${var.ENVIRONMENT}${var.PROJECT}"
  USERNAME_ATTRIBUTES          = ["email"]
  ALLOW_ADMIN_CREATE_USER_ONLY = true

  SCHEMA = [{
    attribute_data_type      = "String"
    mutable                  = true
    name                     = "email"
    required                 = true
    developer_only_attribute = false
    string_attribute_constraints = [{
      max_length = "2048"
      min_length = "0"
    }]
    },
    {
      name                     = "given_name"
      attribute_data_type      = "String"
      mutable                  = true
      required                 = true
      developer_only_attribute = false
      string_attribute_constraints = [{
        max_length = "2048"
        min_length = "0"
      }]
    },
    {
      name                     = "family_name"
      attribute_data_type      = "String"
      mutable                  = true
      required                 = true
      developer_only_attribute = false
      string_attribute_constraints = [{
        max_length = "2048"
        min_length = "0"
      }]
    }
  ]
}

resource "aws_cognito_user_pool_client" "jenkins_app_client" {
  name                                 = "jenkins"
  user_pool_id                         = module.cognito.id
  supported_identity_providers         = ["${aws_cognito_identity_provider.idemia_ad_provider.provider_name}"]
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_scopes                 = ["email", "openid", "profile"]
  callback_urls                        = ["https://jenkins.${var.DNS_ZONE_NAME}/securityRealm/finishLogin"]
  logout_urls                          = ["https://jenkins.${var.DNS_ZONE_NAME}/securityRealm/finishLogin"]
  prevent_user_existence_errors        = "ENABLED"
  refresh_token_validity               = 7
  allowed_oauth_flows_user_pool_client = true
  generate_secret                      = true
}

resource "aws_cognito_user_pool_client" "sonar_app_client" {
  name                                 = "sonar"
  user_pool_id                         = module.cognito.id
  supported_identity_providers         = ["${aws_cognito_identity_provider.idemia_ad_provider.provider_name}"]
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_scopes                 = ["email", "openid", "profile"]
  callback_urls                        = ["https://sonar.${var.DNS_ZONE_NAME}/oauth2/callback/oidc"]
  logout_urls                          = ["https://sonar.${var.DNS_ZONE_NAME}/session/logout"]
  prevent_user_existence_errors        = "ENABLED"
  refresh_token_validity               = 7
  allowed_oauth_flows_user_pool_client = true
  generate_secret                      = true
}

resource "aws_cognito_user_pool_client" "grafana_app_client" {
  name                                 = "grafana"
  user_pool_id                         = module.cognito.id
  supported_identity_providers         = ["${aws_cognito_identity_provider.idemia_ad_provider.provider_name}"]
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_scopes                 = ["email", "openid", "profile"]
  callback_urls                        = ["https://grafana.${var.DNS_ZONE_NAME}/login/generic_oauth"]
  logout_urls                          = ["https://grafana.${var.DNS_ZONE_NAME}/logout"]
  prevent_user_existence_errors        = "ENABLED"
  refresh_token_validity               = 7
  allowed_oauth_flows_user_pool_client = true
  generate_secret                      = true
}

resource "aws_cognito_identity_provider" "idemia_ad_provider" {
  user_pool_id  = module.cognito.id
  provider_name = "idemia"
  provider_type = "OIDC"

  provider_details = {
    authorize_scopes          = "openid"
    client_id                 = "${var.COGNITO_API_KEY}"
    client_secret             = "${var.COGNITO_API_SECRET}"
    attributes_request_method = "GET"
    oidc_issuer               = "https://cognito-idp.eu-west-1.amazonaws.com/${var.USER_POOL_ID}"
    authorize_url             = "${var.FEDERATED_COGNITO_DOMAIN}/oauth2/authorize"
    token_url                 = "${var.FEDERATED_COGNITO_DOMAIN}/oauth2/token"
    attributes_url            = "${var.FEDERATED_COGNITO_DOMAIN}/oauth2/userInfo"
    jwks_uri                  = "https://cognito-idp.eu-west-1.amazonaws.com/${var.USER_POOL_ID}/.well-known/jwks.json"
  }

  attribute_mapping = {
    email              = "email"
    family_name        = "family_name"
    given_name         = "given_name"
    username           = "sub"
    preferred_username = "email"
    name               = "name"
  }
}
