resource "aws_service_discovery_private_dns_namespace" "ecs_service_discovery" {
  name        = "cloud.ci"
  description = "Service discovery for ECS cluster"
  vpc         = module.networking.vpc_id
}

resource "aws_service_discovery_service" "jenkins_master_service" {
  name = "jenkins-master"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.ecs_service_discovery.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_service_discovery_service" "sonarqube_service" {
  name = "sonarqube"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.ecs_service_discovery.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_service_discovery_service" "anchore_service" {
  name = "anchore"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.ecs_service_discovery.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_service_discovery_service" "nist_service" {
  name = "nist"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.ecs_service_discovery.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}

resource "aws_service_discovery_service" "grafana_service" {
  name = "grafana"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.ecs_service_discovery.id

    dns_records {
      ttl  = 10
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 1
  }
}