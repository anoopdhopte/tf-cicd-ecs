resource "aws_appmesh_mesh" "cloudmesh" {
  name = var.PROJECT

  spec {
    egress_filter {
      type = var.egress_filter_type
    }
  }
}

output "mesh_arn" {
  value = join("", aws_appmesh_mesh.cloudmesh.*.arn)
}

output "mesh_created_date" {
  value = join("", aws_appmesh_mesh.cloudmesh.*.created_date)
}

output "mesh_last_updated_date" {
  value = join("", aws_appmesh_mesh.cloudmesh.*.last_updated_date)
}