variable "AWS_REGION" {
  default     = "us-east-1"
  description = "Provide aws region"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "ENVIRONMENT" {
  default     = "test"
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
}

variable "PROJECT" {
  description = "Provide name of the project (max 20 characters)"
}

variable "VPC_CIDR_BLOCK" {
  default     = "10.1.0.0/24"
  description = "Provide cidr block for whole vpc"
}

variable "USE_NAT_GATEWAY" {
  default     = true
  description = "Define whether nat gateway should be used (valid when SKIP_NAT_ON_REGION is not true)"
  type        = bool
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
}

variable "CREATE_ZONE_CERTIFICATE" {
  description = "Whether zone dns wildcard certificate should be created"
  default     = false
}

variable "AWS_ACCOUNT_ID" {
  description = "AWS account ID"
  default     = ""
}

#Variable that contains number of availability zones used by this deployment.
variable "AZ_COUNT" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "EXPIRATION_DAYS" {
  default     = 30
  description = "Number of days after which objects will be removed from bucket."
}

variable "ADMIN_ADDRESS_USERNAME" {
  type        = string
  description = "Username used as first part of admin e-mail address"
  default     = "jenkins"
}

variable "ADMIN_USERS_ID" {
  type        = list(string)
  description = "Emails of admin users"
  default     = []
}


################################################################################
# Jenkins Master
################################################################################
variable "JENKINS_MASTER" {
  description = "Variable describing jenkins-master deployment"
  type        = map(any)
  default = {
    INTERNAL_PORT        = 8080
    EXTERNAL_PORT        = 443
    JNLP_PORT            = 50000
    INSTANCES            = 1
    CPU_SHARES           = "4096"
    MEMORY               = "16384"
    HEALTH_CHECK_TIMEOUT = 5
  }
}

variable "JENKINS_MASTER_IMAGE" {
  description = "Image for jenkins master"
  type        = string
  default     = ""
}

################################################################################
# Jenkins slave
################################################################################
variable "JENKINS_SLAVE" {
  description = "Variable describing jenkins-slave deployment"
  type        = map(any)
  default = {
    CPU_SHARES = "2048"
    MEMORY     = "4096"
  }
}

variable "JENKINS_SLAVE_IMAGE" {
  description = "Image for jenkins slave"
  type        = string
  default     = ""
}

################################################################################
# Squid
################################################################################
variable "SQUID" {
  description = "Variable describing squid deployment"
  type        = map(any)
  default = {
    INTERNAL_PORT = 3128
    EXTERNAL_PORT = 3128
    INSTANCES     = 1
    CPU_SHARES    = "256"
    MEMORY        = "512"
  }
}

variable "SQUID_IMAGE" {
  description = "Image for squid"
  type        = string
  default     = ""
}

################################################################################
# Anchore
################################################################################
variable "ANCHORE" {
  description = "Variable describing squid deployment"
  type        = map(any)
  default = {
    INTERNAL_PORT = 8228
    EXTERNAL_PORT = 8228
    INSTANCES     = 1
    CPU_SHARES    = "2048"
    MEMORY        = "4096"
  }
}

variable "ANCHORE_IMAGE" {
  description = "Image for anchore"
  type        = string
  default     = ""
}

################################################################################
# SonarQube
################################################################################
variable "SONARQUBE" {
  description = "Variable describing squid deployment"
  type        = map(any)
  default = {
    INTERNAL_PORT = 9000
    EXTERNAL_PORT = 9000
    INSTANCES     = 0
    CPU_SHARES    = "1024"
    MEMORY        = "8192"
  }
}

variable "SONARQUBE_IMAGE" {
  description = "Image for sonarqube"
  type        = string
  default     = ""
}

################################################################################
# Nist-sync
################################################################################
variable "NIST_SYNC" {
  description = "Variable describing squid deployment"
  type        = map(any)
  default = {
    INTERNAL_PORT = 80
    EXTERNAL_PORT = 80
    INSTANCES     = 1
    CPU_SHARES    = "512"
    MEMORY        = "1024"
  }
}

variable "NIST_SYNC_IMAGE" {
  description = "Image for nist-sync"
  type        = string
  default     = ""
}

################################################################################
# DB
################################################################################

variable "SONAR_DB_USER" {
  type    = string
  default = "sonar"
}

variable "ANCHORE_DB_USER" {
  type    = string
  default = "anchore"
}

variable "GRAFANA_DB_USER" {
  type    = string
  default = "grafana"
}

################################################################################
# COGNITO
################################################################################
variable "COGNITO_API_KEY" {
  type        = string
  default     = ""
  description = "App api key for integration with corporate cognito"
}

variable "COGNITO_API_SECRET" {
  type        = string
  default     = ""
  description = "App api key secret for integration with corporate cognito"
}

###################################################################################
## APP MESH
###################################################################################
variable "service_discovery_private_namespace_name" {
  default = "cluster.local"
}

variable "egress_filter_type" {
  default     = "DROP_ALL"
  description = "The egress filter type. By default, the type is DROP_ALL. Valid values are ALLOW_ALL and DROP_ALL"
}

variable "protocol" {
  default     = "http"
  description = "protocol for virtual node http/tcp"
}
variable "FEDERATED_COGNITO_DOMAIN" {
  type        = string
  default     = ""
  description = "A federated cognito public url"
}

variable "USER_POOL_ID" {
  type        = string
  default     = ""
  description = "A user pool id"
}

###################################################################################
## DOCKER DAEMON
###################################################################################
variable "DOCKER_DAEMON_VOLUME_SIZE" {
  type        = number
  default     = 8
  description = "Size (GB) of volume attached do EC2 with Docker daemon"
}

###################################################################################
## AMAZON SES
###################################################################################

variable "SES_EMAIL_IDENTITIES" {
  type        = list(string)
  description = "Email addresses that (after verification) can receive messages from SES domain"
  default     = []
}

################################################################################
# Grafana
################################################################################
variable "GRAFANA" {
  description = "Variable describing Grafana deployment"
  type        = map(any)
  default = {
    INTERNAL_PORT = 3000
    EXTERNAL_PORT = 3000
    INSTANCES     = 1
    CPU_SHARES    = "512"
    MEMORY        = "4096"
  }
}

variable "GRAFANA_IMAGE" {
  description = "Image for Grafana"
  type        = string
  default     = ""
}

variable "OFFICE_ACCESS_CIDRS" {
  description = "CIDR blocks of office"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "EXTRA_GRAFANA_ACCESS_CIDRS" {
  description = "Additional CIDR blocks which will have access to Grafana apart from office CIDRs"
  default     = []
  type        = list(string)
}