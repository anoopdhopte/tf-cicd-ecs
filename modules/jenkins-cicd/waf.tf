module "waf_regional" {
  source     = "git::https://bitbucket.org/anoopdhopte/waf.git//modules/regional?ref=1.0.3"
  globals    = module.common.globals
  WAF_PREFIX = "cloud-cicd-waf"
  ALB_ARN    = [aws_alb.anchore_alb_external.arn, aws_alb.jenkins_master_alb_external.arn, aws_alb.sonar_alb_external.arn]
  # Validate if this action will not break your environment
  #RULE_SIZE_RESTRICTION_ACTION_TYPE = "BLOCK"
  #RULE_SQLI_ACTION_TYPE             = "BLOCK"
  #RULE_XSS_ACTION_TYPE              = "BLOCK"
  #RULE_LFI_RFI_ACTION_TYPE          = "BLOCK"
  #RULE_SSI_ACTION_TYPE              = "BLOCK"
  #RULE_AUTH_TOKENS_ACTION_TYPE      = "BLOCK"
  #RULE_ADMIN_ACCESS_ACTION_TYPE     = "BLOCK"
  #RULE_PHP_INSECURITIES_ACTION_TYPE = "BLOCK"
  #RULE_CSRF_ACTION_TYPE             = "BLOCK"
  #RULE_BLACKLISTED_IPS_ACTION_TYPE  = "BLOCK"
}

output "web_acl_metric_name" {
  value = module.waf_regional.web_acl_metric_name
}
