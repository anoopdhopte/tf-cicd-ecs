data "aws_ami" "latest_amazon_linux2" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}

resource "aws_instance" "docker_daemon" {
  count                  = 1
  ami                    = data.aws_ami.latest_amazon_linux2.id
  instance_type          = "t2.medium"
  subnet_id              = module.networking.application_subnet_ids[0]
  vpc_security_group_ids = [aws_security_group.docker_ec2_sg.id, aws_security_group.jenkins_slave.id]

  root_block_device {
    volume_size = var.DOCKER_DAEMON_VOLUME_SIZE
  }

  user_data = <<EOF
#!/bin/bash
set -o xtrace

sudo su
yum update -y
amazon-linux-extras install -y docker

sysctl -w vm.max_map_count=262144
sysctl -w fs.file-max=100000

echo 'vm.max_map_count=262144' >> /etc/sysctl.conf
echo 'fs.file-max=100000' >> /etc/sysctl.conf
sysctl -p

usermod -a -G docker ec2-user

echo "OPTIONS='--default-ulimit nofile=65536:65536 -H tcp://0.0.0.0:2375'" >> /etc/sysconfig/docker

service docker start
EOF

  tags = {
    Terraform               = "true"
    Environment             = var.ENVIRONMENT
    "cpt:tech:autoshutdown" = "false"
  }
}

resource "aws_security_group" "docker_ec2_sg" {
  name        = "testdocker-ec2-sg"
  description = "allow inbound access via docker socket port"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "docker_ec2_2375" {
  security_group_id = aws_security_group.docker_ec2_sg.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 2375
  to_port           = 2375
  cidr_blocks       = ["0.0.0.0/0"]
}
