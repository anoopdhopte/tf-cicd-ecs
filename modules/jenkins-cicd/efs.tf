module "efs_storage_jenkins_master" {
  source       = "git::https://bitbucket.org/anoopdhopte/efs.git//modules?ref=1.0.3"
  globals      = module.common.globals
  KMS_KEY_ID   = module.encryption.kms_key_id
  networking   = module.networking.common
  SUBNET_IDS   = module.networking.application_subnet_ids
  EFS_REPLICAS = length([cidrsubnet(var.VPC_CIDR_BLOCK, 2, var.AZ_COUNT), cidrsubnet(var.VPC_CIDR_BLOCK, 2, var.AZ_COUNT + 1)])
  VOLUME_NAME  = "jenkins-master"
}

module "efs_storage_jenkins_master_backup" {
  source             = "git::https://bitbucket.org/anoopdhopte/backup.git//modules?ref=1.0.0"
  globals            = module.common.globals
  KMS_KEY_ARN        = module.encryption.kms_key_id
  SCHEDULE           = "0 3 * * ? *"
  COLD_STORAGE_AFTER = 5
  DELETE_AFTER       = 95
  BACKUP_RESOURCES = [
    "${module.efs_storage_jenkins_master.efs_arn}"
  ]
}

resource "aws_efs_access_point" "jenkins" {
  file_system_id = module.efs_storage_jenkins_master.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/var/lib/jenkins"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}

module "efs_storage_jenkins_slave" {
  source       = "git::https://bitbucket.org/anoopdhopte/efs.git//modules?ref=1.0.3"
  globals      = module.common.globals
  KMS_KEY_ID   = module.encryption.kms_key_id
  networking   = module.networking.common
  SUBNET_IDS   = module.networking.application_subnet_ids
  EFS_REPLICAS = length([cidrsubnet(var.VPC_CIDR_BLOCK, 2, var.AZ_COUNT), cidrsubnet(var.VPC_CIDR_BLOCK, 2, var.AZ_COUNT + 1)])
  VOLUME_NAME  = "jenkins-worker"
}

resource "aws_efs_access_point" "home" {
  file_system_id = module.efs_storage_jenkins_slave.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/opt/app/\\.m2"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}

resource "aws_efs_access_point" "maven_cache" {
  file_system_id = module.efs_storage_jenkins_slave.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/opt/app/\\.m2/repository"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}

resource "aws_efs_access_point" "owasp_cache" {
  file_system_id = module.efs_storage_jenkins_slave.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/opt/app/\\.m2/owasp"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}

resource "aws_efs_access_point" "owasp_yarn_cache" {
  file_system_id = module.efs_storage_jenkins_slave.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/opt/app/owasp_yarn"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}


resource "aws_efs_access_point" "sdkman_cache" {
  file_system_id = module.efs_storage_jenkins_slave.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/opt/app/\\.sdkman/archives"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}

resource "aws_efs_access_point" "tfenv_cache" {
  file_system_id = module.efs_storage_jenkins_slave.efs_id

  posix_user {
    gid = "499"
    uid = "499"
  }

  root_directory {
    path = "/opt/app/\\.tfenv/versions"
    creation_info {
      owner_gid   = "499"
      owner_uid   = "499"
      permissions = "2775"
    }
  }
}
