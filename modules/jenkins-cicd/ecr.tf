resource "aws_ecr_repository" "ecr_jenkins_master" {
  name                 = "jenkins-master"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository" "ecr_squid" {
  name                 = "squid"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository" "ecr_jenkins_worker" {
  name                 = "jenkins-worker"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository" "ecr_sonarqube" {
  name                 = "sonarqube"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository" "ecr_anchore" {
  name                 = "anchore"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository" "ecr_nist_sync" {
  name                 = "nist-sync"
  image_tag_mutability = "MUTABLE"
}
