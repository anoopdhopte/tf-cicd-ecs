### ALB
resource "aws_alb" "jenkins_master_alb_external" {
  name            = "jenkins-master-lb-external"
  subnets         = module.networking.public_subnet_ids
  security_groups = [aws_security_group.jenkins_master_external_access_sg.id]
}

resource "aws_alb_target_group" "jenkins_master_http_target_group" {
  name        = "jenkins-master-http-external-tg"
  port        = var.JENKINS_MASTER.INTERNAL_PORT
  protocol    = "HTTP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"
  health_check {
    path                = "/login"
    matcher             = "200,302"
    unhealthy_threshold = var.JENKINS_MASTER.HEALTH_CHECK_TIMEOUT
    interval            = 60
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "jenkins_master_listener_external" {
  load_balancer_arn = aws_alb.jenkins_master_alb_external.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = module.jenkins_master_certificate.certificate_arn

  default_action {
    target_group_arn = aws_alb_target_group.jenkins_master_http_target_group.id
    type             = "forward"
  }
}

##redirect rule
resource "aws_lb_listener_rule" "script_block" {
  listener_arn = aws_alb_listener.jenkins_master_listener_external.arn

  action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "NO PERMISSION"
      status_code  = "404"
    }
  }

  condition {
    path_pattern {
      values = ["/script"]
    }
  }
}

resource "aws_lb_listener_rule" "external_prometheus_endpoint_rule" {
  listener_arn = aws_alb_listener.jenkins_master_listener_external.arn

  action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not found"
      status_code  = "404"
    }
  }

  condition {
    path_pattern {
      values = ["/prometheus*"]
    }
  }
}

# JNLP and internal
resource "aws_alb" "jenkins_master_alb_internal" {
  name               = "jenkins-master-lb-internal"
  load_balancer_type = "network"
  internal           = true
  subnets            = module.networking.public_subnet_ids
}

resource "aws_alb_target_group" "jenkins_master_jnlp_target_group" {
  name        = "jenkins-master-internal-jnlp-tg"
  port        = var.JENKINS_MASTER.JNLP_PORT
  protocol    = "TCP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "jenkins_master_jnlp_listener_internal" {
  load_balancer_arn = aws_alb.jenkins_master_alb_internal.id
  port              = var.JENKINS_MASTER.JNLP_PORT
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_alb_target_group.jenkins_master_jnlp_target_group.id
    type             = "forward"
  }
}

resource "aws_alb_target_group" "jenkins_master_http_internal_target_group" {
  name        = "jenkins-master-internal-http-tg"
  port        = var.JENKINS_MASTER.INTERNAL_PORT
  protocol    = "TCP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "jenkins_master_tcp_listener_internal" {
  load_balancer_arn = aws_alb.jenkins_master_alb_internal.id
  port              = var.JENKINS_MASTER.INTERNAL_PORT
  protocol          = "TCP"

  default_action {
    target_group_arn = aws_alb_target_group.jenkins_master_http_internal_target_group.id
    type             = "forward"
  }
}

### Sonar
resource "aws_alb" "sonar_alb_external" {
  name            = "sonar-lb-external"
  subnets         = module.networking.public_subnet_ids
  security_groups = [aws_security_group.jenkins_master_external_access_sg.id]
}

resource "aws_alb_target_group" "sonar_target_group_external" {
  name        = "sonar-tg-external"
  port        = var.SONARQUBE.INTERNAL_PORT
  protocol    = "HTTP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"
  health_check {
    path    = "/api/system/status"
    matcher = "200"
  }
}

resource "aws_alb_listener" "sonar_listener_external" {
  load_balancer_arn = aws_alb.sonar_alb_external.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = module.sonar_certificate.certificate_arn

  default_action {
    target_group_arn = aws_alb_target_group.sonar_target_group_external.id
    type             = "forward"
  }
}

// TODO remove that - go for App Mesh
resource "aws_alb" "sonar_alb_internal" {
  name            = "sonar-lb-internal"
  internal        = true
  subnets         = module.networking.public_subnet_ids
  security_groups = [aws_security_group.nist_mirror.id]
}

resource "aws_alb_target_group" "sonar_target_group_internal" {
  name        = "sonar-tg-internal"
  port        = var.SONARQUBE.INTERNAL_PORT
  protocol    = "HTTP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"
  health_check {
    path    = "/api/system/status"
    matcher = "200"
  }
}

resource "aws_alb_listener" "sonar_listener_internal" {
  load_balancer_arn = aws_alb.sonar_alb_internal.id
  port              = 80
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_alb_target_group.sonar_target_group_internal.id
    type             = "forward"
  }
}

### Anchore
resource "aws_alb" "anchore_alb_external" {
  name            = "anchore-lb"
  subnets         = module.networking.public_subnet_ids
  security_groups = [aws_security_group.jenkins_master_external_access_sg.id]
}

resource "aws_alb_target_group" "anchore_target_group" {
  name        = "anchore-tg"
  port        = var.ANCHORE.INTERNAL_PORT
  protocol    = "HTTP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"

  health_check {
    path = "/health"

  }

}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "anchore_listener_external" {
  load_balancer_arn = aws_alb.anchore_alb_external.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = module.anchore_certificate.certificate_arn

  default_action {
    target_group_arn = aws_alb_target_group.anchore_target_group.id
    type             = "forward"
  }
}

### Nist
resource "aws_alb" "nist_alb_internal" {
  internal        = true
  name            = "nist-lb"
  subnets         = module.networking.public_subnet_ids
  security_groups = [aws_security_group.nist_mirror.id]
}

resource "aws_alb_target_group" "nist_target_group" {
  name        = "nist-tg"
  port        = var.NIST_SYNC.INTERNAL_PORT
  protocol    = "HTTP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"

  health_check {
    path = "/nvdcve-1.0-modified.meta"
  }

}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "nist-listener-internal" {
  load_balancer_arn = aws_alb.nist_alb_internal.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.nist_target_group.id
    type             = "forward"
  }
}

### Grafana
resource "aws_alb" "grafana_alb_external" {
  name            = "grafana-lb-external"
  subnets         = module.networking.public_subnet_ids
  security_groups = [aws_security_group.grafana_external_access.id]
}

resource "aws_alb_target_group" "grafana_http_target_group" {
  name        = "grafana-http-external-tg"
  port        = var.GRAFANA.EXTERNAL_PORT
  protocol    = "HTTP"
  vpc_id      = module.networking.vpc_id
  target_type = "ip"

  health_check {
    path    = "/api/health"
    matcher = "200"
  }
}

resource "aws_alb_listener" "grafana_listener_external" {
  load_balancer_arn = aws_alb.grafana_alb_external.id
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = module.grafana_certificate.certificate_arn

  default_action {
    target_group_arn = aws_alb_target_group.grafana_http_target_group.id
    type             = "forward"
  }
}
