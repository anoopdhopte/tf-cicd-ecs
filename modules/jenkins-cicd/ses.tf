module "ses_domain" {
  source           = "git::https://bitbucket.org/anoopdhopte/ses.git//modules?ref=1.0.0"
  DOMAIN           = var.DNS_ZONE_NAME
  MAIL_FROM_DOMAIN = "jenkins.${var.DNS_ZONE_NAME}"
  ZONE_ID          = module.dns_zone.zone_id
}

data "aws_iam_user" "ses_user" {
  user_name = "cpt-ses-user"
}

resource "aws_iam_access_key" "ses_user_access_key" {
  user = data.aws_iam_user.ses_user.user_name
}

resource "aws_iam_policy" "send_email_policy" {
  name   = "send-email"
  policy = <<-EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ses:SendEmail",
        "ses:SendRawEmail"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:ses:${var.AWS_REGION}:${var.AWS_ACCOUNT_ID}:identity/*"
    }
  ]
}
  EOF
}

resource "aws_iam_user_policy_attachment" "send_email_policy_attachment" {
  user       = data.aws_iam_user.ses_user.user_name
  policy_arn = aws_iam_policy.send_email_policy.arn
}

resource "aws_ses_email_identity" "email_identity" {
  for_each = toset([for v in var.SES_EMAIL_IDENTITIES : tostring(v)])
  email    = each.value
}