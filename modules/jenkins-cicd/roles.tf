################################################################################
# Task execution role
# - allow to pull container images from ECR
# - allow to create log stream and log groups
# - allow to send logs to CloudWatch
################################################################################
resource "aws_iam_role" "ecs_task_execution_role" {
  name = "ECSTaskExecutionRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
          "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

data "aws_iam_policy" "ecs_task_execution_role_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_policy" "ecs_task_create_log_group_policy" {
  name   = "ECSTaksCreateLogGroup"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_task_execution_policy" {
  role       = aws_iam_role.ecs_task_execution_role.id
  policy_arn = data.aws_iam_policy.ecs_task_execution_role_policy.id
}

resource "aws_iam_role_policy_attachment" "attach_task_create_log_group_policy" {
  role       = aws_iam_role.ecs_task_execution_role.id
  policy_arn = aws_iam_policy.ecs_task_create_log_group_policy.id
}

################################################################################
# Jenkins master role
# - management of ECS Tasks
################################################################################
resource "aws_iam_role" "jenkins_master_role" {
  name = "JenkinsMasterRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
          "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

resource "aws_iam_policy" "ecs_task_management_policy" {
  name   = "ECSTaskManagementPolicy"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecs:DescribeTaskDefinition",
                "ecs:ListTaskDefinitionFamilies",
                "ecs:RegisterTaskDefinition",
                "ecs:ListTaskDefinitions",
                "ecs:ListClusters",
                "ecs:DescribeContainerInstances",
                "ecs:DeregisterTaskDefinition",
                "ecs:RunTask"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "ecs:*",
            "Resource": [
                "arn:aws:ecs:*:*:task-set/*/*/*",
                "arn:aws:ecs:*:*:task/*",
                "arn:aws:ecs:*:*:container-instance/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy" "aws_secretsmanager_policy" {
  name   = "AWSSecretsManagerPolicy"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds",
                "secretsmanager:ListSecrets"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy" "pass_role_policy" {
  name   = "PassRole"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Action": [
            "iam:GetRole",
            "iam:PassRole"
        ],
        "Resource": "${aws_iam_role.ecs_task_execution_role.arn}"
    }]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_ecs_task_management_policy" {
  role       = aws_iam_role.jenkins_master_role.id
  policy_arn = aws_iam_policy.ecs_task_management_policy.id
}

resource "aws_iam_role_policy_attachment" "attach_aws_secretsmanager_policy" {
  role       = aws_iam_role.jenkins_master_role.id
  policy_arn = aws_iam_policy.aws_secretsmanager_policy.id
}

resource "aws_iam_role_policy_attachment" "attach_pass_role_policy" {
  role       = aws_iam_role.jenkins_master_role.id
  policy_arn = aws_iam_policy.pass_role_policy.id
}

################################################################################
# RDS access role
# - access to RDS
################################################################################
resource "aws_iam_role" "rds_access_role" {
  name = "RDSAccessRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
          "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

data "aws_iam_policy" "rds_access_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonRDSDataFullAccess"
}

resource "aws_iam_role_policy_attachment" "attach_rds_access_policy" {
  role       = aws_iam_role.rds_access_role.id
  policy_arn = data.aws_iam_policy.rds_access_policy.id
}

################################################################################
# Anchore role
# - access to RDS
# - access to ECR
################################################################################
resource "aws_iam_role" "anchore" {
  name = "Anchore"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
          "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

data "aws_iam_policy" "anchore_rds_access_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonRDSDataFullAccess"
}

resource "aws_iam_role_policy_attachment" "attach_anchore_rds_access_policy" {
  role       = aws_iam_role.anchore.id
  policy_arn = data.aws_iam_policy.anchore_rds_access_policy.id
}


data "aws_iam_policy" "anchore_ecr_access_policy" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_role_policy_attachment" "attach_anchore_ecr_access_policy" {
  role       = aws_iam_role.anchore.id
  policy_arn = data.aws_iam_policy.anchore_ecr_access_policy.id
}

######
# GRAFANA
resource "aws_iam_policy" "grafana_policy" {
  name   = "Grafana"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowReadingMetricsFromCloudWatch",
      "Effect": "Allow",
      "Action": [
        "cloudwatch:DescribeAlarmsForMetric",
        "cloudwatch:DescribeAlarmHistory",
        "cloudwatch:DescribeAlarms",
        "cloudwatch:ListMetrics",
        "cloudwatch:GetMetricStatistics",
        "cloudwatch:GetMetricData"
      ],
      "Resource": "*"
    },
    {
      "Sid": "AllowReadingLogsFromCloudWatch",
      "Effect": "Allow",
      "Action": [
        "logs:DescribeLogGroups",
        "logs:GetLogGroupFields",
        "logs:StartQuery",
        "logs:StopQuery",
        "logs:GetQueryResults",
        "logs:GetLogEvents"
      ],
      "Resource": "*"
    },
    {
      "Sid": "AllowReadingTagsInstancesRegionsFromEC2",
      "Effect": "Allow",
      "Action": ["ec2:DescribeTags", "ec2:DescribeInstances", "ec2:DescribeRegions"],
      "Resource": "*"
    },
    {
      "Sid": "AllowReadingResourcesForTags",
      "Effect": "Allow",
      "Action": "tag:GetResources",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "grafana_role" {
  name = "GrafanaRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
          "Service": "ecs-tasks.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_grafana_access_policy" {
  role       = aws_iam_role.grafana_role.id
  policy_arn = aws_iam_policy.grafana_policy.arn
}
