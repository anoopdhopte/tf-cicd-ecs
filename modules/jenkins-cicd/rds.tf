module "postgresql_sonar" {
  source                          = "git::https://bitbucket.org/anoopdhopte/rds.git//modules/instance?ref=2.0.0"
  networking                      = module.networking.common
  globals                         = module.common.globals
  ENGINE                          = "postgres"
  ENGINE_VERSION                  = "9.6.22"
  KMS_KEY_ID                      = module.encryption.kms_key_id
  NAME                            = "sonar"
  USERNAME                        = var.SONAR_DB_USER
  PASSWORD                        = random_password.sonar_db_password.result
  DELETION_PROTECTION             = true
  RDS_ACCESS_CIDR                 = [module.networking.vpc_cidr_block]
  ENABLED_CLOUDWATCH_LOGS_EXPORTS = ["postgresql", "upgrade"]
  SUBNET_IDS                      = module.networking.data_subnet_ids
  ALLOW_MAJOR_VERSION_UPGRADE     = false
}

module "postgresql_anchore" {
  source                          = "git::https://bitbucket.org/anoopdhopte/rds.git//modules/instance?ref=2.0.0"
  networking                      = module.networking.common
  globals                         = module.common.globals
  ENGINE                          = "postgres"
  ENGINE_VERSION                  = "9.6.22"
  KMS_KEY_ID                      = module.encryption.kms_key_id
  NAME                            = "anchore"
  USERNAME                        = var.ANCHORE_DB_USER
  PASSWORD                        = random_password.anchore_db_password.result
  DELETION_PROTECTION             = true
  RDS_ACCESS_CIDR                 = [module.networking.vpc_cidr_block]
  ENABLED_CLOUDWATCH_LOGS_EXPORTS = ["postgresql", "upgrade"]
  SUBNET_IDS                      = module.networking.data_subnet_ids
  ALLOW_MAJOR_VERSION_UPGRADE     = false
}

module "postgresql_grafana" {
  source                          = "git::https://bitbucket.org/anoopdhopte/rds.git//modules/instance?ref=2.0.0"
  networking                      = module.networking.common
  globals                         = module.common.globals
  ENGINE                          = "postgres"
  ENGINE_VERSION                  = "9.6.22"
  KMS_KEY_ID                      = module.encryption.kms_key_id
  NAME                            = "grafana"
  USERNAME                        = var.GRAFANA_DB_USER
  PASSWORD                        = random_password.grafana_db_password.result
  DELETION_PROTECTION             = true
  RDS_ACCESS_CIDR                 = [module.networking.vpc_cidr_block]
  ENABLED_CLOUDWATCH_LOGS_EXPORTS = ["postgresql", "upgrade"]
  SUBNET_IDS                      = module.networking.data_subnet_ids
  ALLOW_MAJOR_VERSION_UPGRADE     = false
}
