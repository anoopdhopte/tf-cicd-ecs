# Fetch AZs in the current region
data "aws_availability_zones" "available_zones" {}

resource "random_pet" "secrets_manager_name" {
}

resource "random_pet" "ecs_cluster_name" {
}

resource "random_password" "sonar_db_password" {
  length           = 15
  min_lower        = 1
  min_special      = 1
  min_upper        = 1
  min_numeric      = 1
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "anchore_db_password" {
  length           = 15
  min_lower        = 1
  min_special      = 1
  min_upper        = 1
  min_numeric      = 1
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "anchore_admin_password" {
  length      = 15
  min_lower   = 1
  min_special = 1
  min_upper   = 1
  min_numeric = 1
}

resource "random_password" "grafana_db_password" {
  length           = 15
  min_lower        = 1
  min_special      = 1
  min_upper        = 1
  min_numeric      = 1
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "grafana_admin_password" {
  length      = 15
  min_lower   = 1
  min_special = 1
  min_upper   = 1
  min_numeric = 1
}

data "aws_secretsmanager_secret" "sonar_token_secret" {
  name = "sonarToken"
}

data "aws_secretsmanager_secret_version" "sonar_token_secret_version" {
  secret_id = data.aws_secretsmanager_secret.sonar_token_secret.id
}
