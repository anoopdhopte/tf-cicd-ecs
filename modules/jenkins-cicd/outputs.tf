output "common_globals" {
  value = module.common.globals
}

output "vpc_id" {
  value = module.networking.vpc_id
}

output "common_networkig" {
  value = module.networking.common
}

output "networking_public_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.public_route_table_ids
}

output "networking_application_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.application_route_table_ids
}

output "networking_data_route_table_ids" {
  description = "Return networking data_route_table_ids"
  value       = module.networking.data_route_table_ids
}