module "jenkins_master_domain_name" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = aws_alb.jenkins_master_alb_external.dns_name
  ALIAS_ZONE_ID = aws_alb.jenkins_master_alb_external.zone_id
  DOMAIN_NAME   = "jenkins.${var.DNS_ZONE_NAME}"
}

module "authorization_domain_name" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = aws_cognito_user_pool_domain.cognito_domain_name.cloudfront_distribution_arn
  ALIAS_ZONE_ID = "Z2FDTNDATAQYW2"
  DOMAIN_NAME   = "auth.jenkins.${var.DNS_ZONE_NAME}"
}

module "sonar_domain_name" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = aws_alb.sonar_alb_external.dns_name
  ALIAS_ZONE_ID = aws_alb.sonar_alb_external.zone_id
  DOMAIN_NAME   = "sonar.${var.DNS_ZONE_NAME}"
}

module "grafana_domain_name" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = aws_alb.grafana_alb_external.dns_name
  ALIAS_ZONE_ID = aws_alb.grafana_alb_external.zone_id
  DOMAIN_NAME   = "grafana.${var.DNS_ZONE_NAME}"
}
