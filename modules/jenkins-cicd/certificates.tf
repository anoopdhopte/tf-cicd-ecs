module "jenkins_master_certificate" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/certificates?ref=1.1.0"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "jenkins"
}

module "auth_certificate" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/certificates?ref=1.1.0"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "auth.jenkins"
  providers = {
    aws = aws.certificates
    #region = eu-central-1
  }
}

module "sonar_certificate" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/certificates?ref=1.1.0"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "sonar"
}

module "anchore_certificate" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/certificates?ref=1.1.0"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "anchore"
}

module "nist_certificate" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/certificates?ref=1.1.0"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "cvedb"
}

module "grafana_certificate" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/certificates?ref=1.1.0"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = module.dns_zone.zone_id
  SUBDOMAIN     = "grafana"
}
