# Traffic to the ECS Cluster should only come from the ALB
resource "aws_security_group" "jenkins_master_sg" {
  name_prefix = "jenkins-master-sg"
  description = "Jenkins master - allow internal access to jenkins-master task on http on ${var.JENKINS_MASTER.INTERNAL_PORT} and on JNLP on ${var.JENKINS_MASTER.JNLP_PORT}, allow full egress access"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.JENKINS_MASTER.JNLP_PORT
    to_port     = var.JENKINS_MASTER.JNLP_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.JENKINS_MASTER.INTERNAL_PORT
    to_port     = var.JENKINS_MASTER.INTERNAL_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "jenkins_master_external_access_sg" {
  name_prefix = "jenkins-master-external-access-sg"
  description = "Public access from internet on HTTPS port (${var.JENKINS_MASTER.EXTERNAL_PORT}) to jenkins master load balancer, allow full egress access"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "jenkins_slave" {
  name_prefix = "jenkins-slave"
  description = "Jenkins slave - allow full egress access, block all ingress access"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "docker_test_ports" {
  security_group_id = aws_security_group.jenkins_slave.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 30000
  to_port           = 65535
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group" "nist_mirror" {
  name_prefix = "nist-mirror"
  description = "NIST mirror - allow internal access to NIST mirror task on http on ${var.NIST_SYNC.INTERNAL_PORT}, allow full egress access"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.NIST_SYNC.INTERNAL_PORT
    to_port     = var.NIST_SYNC.INTERNAL_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "squid" {
  name_prefix = "squid"
  description = "Squid proxy - allow internal acess to proxy on port ${var.SQUID.INTERNAL_PORT}"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.SQUID.INTERNAL_PORT
    to_port     = var.SQUID.INTERNAL_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "anchore" {
  name_prefix = "anchore"
  description = "Anchore scanner - allow internal flows to anchore scanner on ports ${var.ANCHORE.INTERNAL_PORT}"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.ANCHORE.INTERNAL_PORT
    to_port     = var.ANCHORE.INTERNAL_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "sonarqube" {
  name_prefix = "sonarqube"
  description = "Sonarqube - allow internal communication with Sonarqube on port ${var.SONARQUBE.INTERNAL_PORT}"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.SONARQUBE.INTERNAL_PORT
    to_port     = var.SONARQUBE.INTERNAL_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

#### GRAFANA
resource "aws_security_group" "grafana_external_access" {
  name_prefix = "grafana-external-access-sg"
  description = "Public access from internet on HTTPS port (443) to Grafana load balancer, allow full egress access"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = concat(var.OFFICE_ACCESS_CIDRS, var.EXTRA_GRAFANA_ACCESS_CIDRS)
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "grafana" {
  name_prefix = "grafana-sg"
  description = "Grafana - allow internal access to grafana task on http on ${var.GRAFANA.INTERNAL_PORT}, allow full egress access"
  vpc_id      = module.networking.vpc_id

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = var.GRAFANA.INTERNAL_PORT
    to_port     = var.GRAFANA.INTERNAL_PORT
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}
