### Endpoints for pulling docker images
module "endpoint_ecr_dkr" {
  source        = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/endpoint?ref=2.1.3"
  globals       = module.common.globals
  networking    = module.networking.common
  SERVICE_NAME  = "com.amazonaws.${var.AWS_REGION}.ecr.dkr"
  ENDPOINT_TYPE = "Interface"
  SUBNET_IDS    = module.networking.application_subnet_ids
  //TODO
  //Those add default sg rule are probably to wide permissions
  ADD_DEFAULT_SG_RULE = true
  INGRESS_PORTS       = [443]
  //  AUTO_ACCEPT     = true
}

module "endpoint_ecr_api" {
  source        = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/endpoint?ref=2.1.3"
  globals       = module.common.globals
  networking    = module.networking.common
  SERVICE_NAME  = "com.amazonaws.${var.AWS_REGION}.ecr.api"
  ENDPOINT_TYPE = "Interface"
  SUBNET_IDS    = module.networking.application_subnet_ids
  //TODO
  //Those add default sg rule are probably to wide permissions
  ADD_DEFAULT_SG_RULE = true
  INGRESS_PORTS       = [443]
  //  AUTO_ACCEPT     = true
}

module "endpoint_s3" {
  source          = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/endpoint?ref=2.1.3"
  globals         = module.common.globals
  networking      = module.networking.common
  SERVICE_NAME    = "com.amazonaws.${var.AWS_REGION}.s3"
  ROUTE_TABLE_IDS = concat(module.networking.application_route_table_ids, module.networking.public_route_table_ids)
}

### Endpoints for collecting logs
module "endpoint_logs" {
  source        = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/endpoint?ref=2.1.3"
  globals       = module.common.globals
  networking    = module.networking.common
  SERVICE_NAME  = "com.amazonaws.${var.AWS_REGION}.logs"
  ENDPOINT_TYPE = "Interface"
  SUBNET_IDS    = module.networking.application_subnet_ids
  //TODO
  //Those add default sg rule are probably to wide permissions
  ADD_DEFAULT_SG_RULE = true
  INGRESS_PORTS       = [443]
  //  AUTO_ACCEPT     = true
}

### Endpoints for ECS/Fargate
module "endpoint_ecs" {
  source        = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/endpoint?ref=2.1.3"
  globals       = module.common.globals
  networking    = module.networking.common
  SERVICE_NAME  = "com.amazonaws.${var.AWS_REGION}.ecs"
  ENDPOINT_TYPE = "Interface"
  SUBNET_IDS    = module.networking.application_subnet_ids
  //TODO
  //Those add default sg rule are probably to wide permissions
  ADD_DEFAULT_SG_RULE = true
  INGRESS_PORTS       = [443]
  //  AUTO_ACCEPT     = true
}