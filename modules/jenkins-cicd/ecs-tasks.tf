locals {
  casc_vars = {
    ecs_cluster_arn               = module.ecs_cluster.cluster_arn
    url                           = "https://${module.jenkins_master_domain_name.fqdn}/"
    region                        = var.AWS_REGION
    client_id                     = aws_cognito_user_pool_client.jenkins_app_client.id
    client_secret                 = aws_cognito_user_pool_client.jenkins_app_client.client_secret
    slaves                        = ["jenkins-slave"]
    subnets                       = join(",", module.networking.application_subnet_ids)
    securityGroups                = aws_security_group.jenkins_slave.id
    cognito_url                   = join("", ["https://", aws_cognito_user_pool_domain.cognito_domain_name.domain])
    jenkins_service_discovery_url = join("", ["http://", aws_service_discovery_service.jenkins_master_service.name, ".", aws_service_discovery_private_dns_namespace.ecs_service_discovery.name, ":", var.JENKINS_MASTER.INTERNAL_PORT, "/"])
    anchore_service_discovery_url = join("", ["http://", aws_service_discovery_service.anchore_service.name, ".", aws_service_discovery_private_dns_namespace.ecs_service_discovery.name, ":", var.ANCHORE.INTERNAL_PORT, "/v1"])
    anchore_admin_password        = random_password.anchore_admin_password.result
    sonar_url                     = "https://${module.sonar_domain_name.fqdn}"
    env_name                      = var.ENVIRONMENT
    smtp_host                     = "email-smtp.${var.AWS_REGION}.amazonaws.com"
    smtp_username                 = aws_iam_access_key.ses_user_access_key.id
    smtp_password                 = aws_iam_access_key.ses_user_access_key.ses_smtp_password_v4
    admin_email_address           = "${var.ADMIN_ADDRESS_USERNAME}@${var.DNS_ZONE_NAME}"
    admin_users_whitelist         = indent(8, join("\n", formatlist("- \"Overall/Administer:%s\"", var.ADMIN_USERS_ID)))
    settings_xml                  = indent(12, templatefile("${path.module}/conf/settings.xml", local.settings_vars))
    dependency_check_properties   = file("${path.module}/conf/dependency-check-properties")
  }
  settings_vars = {
    nist_service_discovery_url  = join("", ["http://", aws_service_discovery_service.nist_service.name, ".", aws_service_discovery_private_dns_namespace.ecs_service_discovery.name, ":", var.NIST_SYNC.INTERNAL_PORT, ""])
    sonar_service_discovery_url = join("", ["http://", aws_service_discovery_service.sonarqube_service.name, ".", aws_service_discovery_private_dns_namespace.ecs_service_discovery.name, ":", var.SONARQUBE.INTERNAL_PORT, "/"])
    sonar_token                 = jsondecode(data.aws_secretsmanager_secret_version.sonar_token_secret_version.secret_string).sonartoken
  }
  grafana_vars = {
    region       = var.AWS_REGION
    cluster_name = module.ecs_cluster.cluster_name
  }
}

#Task definition for Jenkins Master instance
resource "aws_ecs_task_definition" "jenkins_master" {
  family                   = "jenkins-master"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.JENKINS_MASTER.CPU_SHARES
  memory                   = var.JENKINS_MASTER.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.jenkins_master_role.arn

  volume {
    name = "jenkins-master"
    efs_volume_configuration {
      file_system_id     = module.efs_storage_jenkins_master.efs_id
      transit_encryption = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.jenkins.id
      }

    }
  }

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.JENKINS_MASTER.CPU_SHARES},
    "image": "${var.JENKINS_MASTER_IMAGE}",
    "memory": ${var.JENKINS_MASTER.MEMORY},
    "name": "jenkins-master",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "jenkins",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "master"
        }
      },
    "networkMode": "awsvpc",
    "mountPoints": [
      {
        "readOnly": false,
        "containerPath": "/var/lib/jenkins",
        "sourceVolume": "jenkins-master"
      }
    ],
    "entryPoint": [
      "sh",
      "-c",
      "echo $CASC_BASE64 | base64 -d > $JENKINS_HOME/jenkins.yaml; /usr/bin/jenkins_master.sh"
    ],
    "environment": [
      { "name" : "CASC_BASE64", "value" : "${base64encode(templatefile("${path.module}/conf/casc.yaml", local.casc_vars))}" },
      { "name" : "JENKINS_PREFIX", "value" : "/" },
      { "name" : "JVM_MINIMUM_MEMORY", "value" : "12288m" },
      { "name" : "JVM_MAXIMUM_MEMORY", "value" : "12288m" }
    ],
    "portMappings": [
      {
        "containerPort": ${var.JENKINS_MASTER.INTERNAL_PORT},
        "hostPort": ${var.JENKINS_MASTER.INTERNAL_PORT}
      },
      {
        "containerPort": ${var.JENKINS_MASTER.JNLP_PORT},
        "hostPort": ${var.JENKINS_MASTER.JNLP_PORT}
      }
    ]
  }
]
DEFINITION

}

# Use terraform managed jenkins worker task definition
# Externally-managed ECS task definition to use, instead of creating task definitions using the Template Name.
resource "aws_ecs_task_definition" "jenkins_slave" {
  family                   = "jenkins-slave"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.JENKINS_SLAVE.CPU_SHARES
  memory                   = var.JENKINS_SLAVE.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn

  volume {
    name = "maven-cache"

    efs_volume_configuration {
      file_system_id     = module.efs_storage_jenkins_slave.efs_id
      transit_encryption = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.maven_cache.id
      }

    }
  }

  volume {
    name = "owasp-cache"

    efs_volume_configuration {
      file_system_id     = module.efs_storage_jenkins_slave.efs_id
      transit_encryption = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.owasp_cache.id
      }

    }
  }
  volume {
    name = "owasp-yarn-cache"

    efs_volume_configuration {
      file_system_id     = module.efs_storage_jenkins_slave.efs_id
      transit_encryption = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.owasp_yarn_cache.id
      }

    }
  }

  volume {
    name = "sdkman-cache"

    efs_volume_configuration {
      file_system_id     = module.efs_storage_jenkins_slave.efs_id
      transit_encryption = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.sdkman_cache.id
      }

    }
  }

  volume {
    name = "tfenv-cache"

    efs_volume_configuration {
      file_system_id     = module.efs_storage_jenkins_slave.efs_id
      transit_encryption = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.tfenv_cache.id
      }

    }
  }

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.JENKINS_SLAVE.CPU_SHARES},
    "image": "${var.JENKINS_SLAVE_IMAGE}",
    "memory": ${var.JENKINS_SLAVE.MEMORY},
    "name": "jenkins-dynamic-worker",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "workers",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "dynamic-worker"
      }
    },
    "networkMode": "awsvpc",
    "environment": [
      { "name" : "DOCKER_HOST", "value" : "tcp://${aws_instance.docker_daemon[0].private_ip}:2375" }
    ],
    "mountPoints": [
      {
        "readOnly": false,
        "containerPath": "/opt/app/.m2/repository",
        "sourceVolume": "maven-cache"
      },
      {
        "readOnly": false,
        "containerPath": "/opt/app/.m2/owasp",
        "sourceVolume": "owasp-cache"
      },
      {
        "readOnly": false,
        "containerPath": "/opt/app/owasp_yarn",
        "sourceVolume": "owasp-yarn-cache"
      },
      {
        "readOnly": false,
        "containerPath": "/opt/app/.sdkman/archives",
        "sourceVolume": "sdkman-cache"
      },
      {
        "readOnly": false,
        "containerPath": "/opt/app/.tfenv/versions",
        "sourceVolume": "tfenv-cache"
      }
    ]
  }
]
DEFINITION

}

#Task definition for Squid-Proxy instance
resource "aws_ecs_task_definition" "squid" {
  family                   = "squid"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.SQUID.CPU_SHARES
  memory                   = var.SQUID.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.SQUID.CPU_SHARES},
    "image": "${var.SQUID_IMAGE}",
    "memory": ${var.SQUID.MEMORY},
    "name": "squid",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "squid",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "squid"
        }
      },
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${var.SQUID.INTERNAL_PORT},
        "hostPort": ${var.SQUID.EXTERNAL_PORT}
      }
    ]
  }
]
DEFINITION

}

/*Task definition for Anchore instance*/

resource "aws_ecs_task_definition" "anchore" {
  family                   = "anchore"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.ANCHORE.CPU_SHARES
  memory                   = var.ANCHORE.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.anchore.arn

  container_definitions = <<DEFINITION
[
  {
    "dependsOn" : [
      {
        "condition" : "START",
        "containerName" : "engine-catalog"
      }
    ],
    "entryPoint": [
      "sh",
      "-c",
      "mkdir ~/.docker; echo $DOCKER_CONFIG_BASE64 | base64 -d > ~/.docker/config.json; /docker-entrypoint.sh anchore-manager service start simplequeue"
    ],
    "environment": [
      {
        "name": "DOCKER_CONFIG_BASE64",
        "value": "${base64encode(file("${path.module}/docker-config.json"))}"
      },
      {
        "name": "ANCHORE_ENDPOINT_HOSTNAME",
        "value": "localhost"
      },
      {
        "name": "ANCHORE_ADMIN_PASSWORD",
        "value": "${random_password.anchore_admin_password.result}"
      },
      {
        "name": "ANCHORE_DB_HOST",
        "value": "${module.postgresql_anchore.db_instance_address[0]}"
      },
      {
        "name": "ANCHORE_DB_USER",
        "value": "${module.postgresql_anchore.db_instance_username[0]}"
      },
      {
        "name": "ANCHORE_DB_NAME",
        "value": "${module.postgresql_anchore.db_instance_name[0]}"
      },
      {
        "name": "ANCHORE_DB_PASSWORD",
        "value": "${random_password.anchore_db_password.result}"
      },
      {
        "name": "ANCHORE_SERVICE_PORT",
        "value": "8232"
      },
      {
        "name": "ANCHORE_EXTERNAL_PORT",
        "value": "8232"
      }
    ],
    "essential": true,
    "image": "${var.ANCHORE_IMAGE}",
    "name": "engine-simpleq",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "anchore",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "anchore"
      }
    },
    "portMappings": [
      {
        "containerPort": 8232,
        "hostPort": 8232
      }
    ]
  },
  {
    "entryPoint": [
      "sh",
      "-c",
      "mkdir ~/.docker; echo $DOCKER_CONFIG_BASE64 | base64 -d > ~/.docker/config.json; /docker-entrypoint.sh anchore-manager service start analyzer"
    ],
    "environment": [
      {
        "name": "DOCKER_CONFIG_BASE64",
        "value": "${base64encode(file("${path.module}/docker-config.json"))}"
      },
      {
        "name": "ANCHORE_ENDPOINT_HOSTNAME",
        "value": "localhost"
      },
      {
        "name": "ANCHORE_ADMIN_PASSWORD",
        "value": "${random_password.anchore_admin_password.result}"
      },
      {
        "name": "ANCHORE_DB_HOST",
        "value": "${module.postgresql_anchore.db_instance_address[0]}"
      },
      {
        "name": "ANCHORE_DB_USER",
        "value": "${module.postgresql_anchore.db_instance_username[0]}"
      },
      {
        "name": "ANCHORE_DB_NAME",
        "value": "${module.postgresql_anchore.db_instance_name[0]}"
      },
      {
        "name": "ANCHORE_DB_PASSWORD",
        "value": "${random_password.anchore_db_password.result}"
      },
      {
        "name": "ANCHORE_SERVICE_PORT",
        "value": "8229"
      },
      {
        "name": "ANCHORE_EXTERNAL_PORT",
        "value": "8229"
      }
    ],
    "essential": true,
    "image": "${var.ANCHORE_IMAGE}",
    "name": "engine-analyzer",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "anchore",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "anchore"
      }
    },
    "dependsOn" : [
      {
        "condition" : "START",
        "containerName" : "engine-catalog"
      }
    ],
    "portMappings": [
      {
        "containerPort": 8229,
        "hostPort": 8229
      }
    ]
  },
  {
    "dependsOn" : [
      {
        "condition" : "START",
        "containerName" : "engine-catalog"
      }
    ],
    "entryPoint": [
      "sh",
      "-c",
      "mkdir ~/.docker; echo $DOCKER_CONFIG_BASE64 | base64 -d > ~/.docker/config.json; /docker-entrypoint.sh anchore-manager service start apiext"
    ],
    "environment": [
      {
        "name": "DOCKER_CONFIG_BASE64",
        "value": "${base64encode(file("${path.module}/docker-config.json"))}"
      },
      {
        "name": "ANCHORE_ENDPOINT_HOSTNAME",
        "value": "localhost"
      },
      {
        "name": "ANCHORE_ADMIN_PASSWORD",
        "value": "${random_password.anchore_admin_password.result}"
      },
      {
        "name": "ANCHORE_DB_HOST",
        "value": "${module.postgresql_anchore.db_instance_address[0]}"
      },
      {
        "name": "ANCHORE_DB_USER",
        "value": "${module.postgresql_anchore.db_instance_username[0]}"
      },
      {
        "name": "ANCHORE_DB_NAME",
        "value": "${module.postgresql_anchore.db_instance_name[0]}"
      },
      {
        "name": "ANCHORE_DB_PASSWORD",
        "value": "${random_password.anchore_db_password.result}"
      }
    ],
    "essential": true,
    "image": "${var.ANCHORE_IMAGE}",
    "name": "engine-api",
    "portMappings": [
      {
        "containerPort": ${var.ANCHORE.INTERNAL_PORT},
        "hostPort": ${var.ANCHORE.EXTERNAL_PORT}
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "anchore",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "anchore"
      }
    }
  },
  {
    "entryPoint": [
      "sh",
      "-c",
      "mkdir ~/.docker; echo $DOCKER_CONFIG_BASE64 | base64 -d > ~/.docker/config.json; /docker-entrypoint.sh anchore-manager service start catalog"
    ],
    "environment": [
      {
        "name": "DOCKER_CONFIG_BASE64",
        "value": "${base64encode(file("${path.module}/docker-config.json"))}"
      },
      {
        "name": "ANCHORE_ENDPOINT_HOSTNAME",
        "value": "localhost"
      },
      {
        "name": "ANCHORE_ADMIN_PASSWORD",
        "value": "${random_password.anchore_admin_password.result}"
      },
      {
        "name": "ANCHORE_DB_HOST",
        "value": "${module.postgresql_anchore.db_instance_address[0]}"
      },
      {
        "name": "ANCHORE_DB_USER",
        "value": "${module.postgresql_anchore.db_instance_username[0]}"
      },
      {
        "name": "ANCHORE_DB_NAME",
        "value": "${module.postgresql_anchore.db_instance_name[0]}"
      },
      {
        "name": "ANCHORE_DB_PASSWORD",
        "value": "${random_password.anchore_db_password.result}"
      },
      {
        "name": "ANCHORE_SERVICE_PORT",
        "value": "8230"
      },
      {
        "name": "ANCHORE_EXTERNAL_PORT",
        "value": "8230"
      }
    ],
    "essential": true,
    "image": "${var.ANCHORE_IMAGE}",
    "name": "engine-catalog",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "anchore",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "anchore"
      }
    },
    "portMappings": [
      {
        "containerPort": 8230,
        "hostPort": 8230
      }
    ]
  },
  {
    "dependsOn" : [
      {
        "condition" : "START",
        "containerName" : "engine-catalog"
      }
    ],
    "entryPoint": [
      "sh",
      "-c",
      "mkdir ~/.docker; echo $DOCKER_CONFIG_BASE64 | base64 -d > ~/.docker/config.json; /docker-entrypoint.sh anchore-manager service start policy_engine"
    ],
    "environment": [
      {
        "name": "DOCKER_CONFIG_BASE64",
        "value": "${base64encode(file("${path.module}/docker-config.json"))}"
      },
      {
        "name": "ANCHORE_ENDPOINT_HOSTNAME",
        "value": "localhost"
      },
      {
        "name": "ANCHORE_ADMIN_PASSWORD",
        "value": "${random_password.anchore_admin_password.result}"
      },
      {
        "name": "ANCHORE_DB_HOST",
        "value": "${module.postgresql_anchore.db_instance_address[0]}"
      },
      {
        "name": "ANCHORE_DB_USER",
        "value": "${module.postgresql_anchore.db_instance_username[0]}"
      },
      {
        "name": "ANCHORE_DB_NAME",
        "value": "${module.postgresql_anchore.db_instance_name[0]}"
      },
      {
        "name": "ANCHORE_DB_PASSWORD",
        "value": "${random_password.anchore_db_password.result}"
      },
      {
        "name": "ANCHORE_SERVICE_PORT",
        "value": "8231"
      },
      {
        "name": "ANCHORE_EXTERNAL_PORT",
        "value": "8231"
      }
    ],
    "essential": true,
    "image": "${var.ANCHORE_IMAGE}",
    "name": "engine-policy-engine",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "anchore",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "anchore"
      }
    },
    "portMappings": [
      {
        "containerPort": 8231,
        "hostPort": 8231
      }
    ]
  }
]
DEFINITION

}

/*Task definition for SonarQube instance*/
resource "aws_ecs_task_definition" "sonarqube" {
  family                   = "SonarQube"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.SONARQUBE.CPU_SHARES
  memory                   = var.SONARQUBE.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.rds_access_role.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.SONARQUBE.CPU_SHARES},
    "image": "${var.SONARQUBE_IMAGE}",
    "memory": ${var.SONARQUBE.MEMORY},
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65535,
        "hardLimit": 65535
      }
    ],
    "name": "sonarqube",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "sonarqube",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "sonarqube"
        }
      },
    "networkMode": "awsvpc",
    "environment": [
      { "name" : "SONARQUBE_JDBC_URL", "value" : "jdbc:postgresql://${module.postgresql_sonar.db_instance_endpoint[0]}/${module.postgresql_sonar.db_instance_name[0]}"},
      { "name" : "SONARQUBE_JDBC_USERNAME", "value" : "${module.postgresql_sonar.db_instance_username[0]}"},
      { "name" : "SONARQUBE_JDBC_PASSWORD", "value" : "${random_password.sonar_db_password.result}"},
      { "name" : "JVM_MINIMUM_MEMORY", "value" : "${var.SONARQUBE.MEMORY}M"},
      { "name" : "JVM_MAXIMUM_MEMORY", "value" : "${var.SONARQUBE.MEMORY}M"},
      { "name" : "SONARQUBE_BASE_URL", "value" : "https://sonar.${var.DNS_ZONE_NAME}"},
      { "name" : "SONARQUBE_OIDC_ENABLED", "value" : "True"},
      { "name" : "SONARQUBE_OIDC_ISSUER_URI", "value" : "https://${module.cognito.endpoint}"},
      { "name" : "SONARQUBE_OIDC_CLIENT_ID", "value" : "${aws_cognito_user_pool_client.sonar_app_client.id}"},
      { "name" : "SONARQUBE_OIDC_CLIENT_SECRET", "value" : "${aws_cognito_user_pool_client.sonar_app_client.client_secret}"},
      { "name" : "SONARQUBE_OIDC_SCROPES", "value" : "openid profile email"},
      { "name" : "SONARQUBE_OIDC_ALLOW_TO_SIGN", "value" : "True"},
      { "name" : "SONARQUBE_OIDC_LOGIN_STRATEGY", "value" : "Email"},
      { "name" : "SONARQUBE_OIDC_CUSTOMCLAIM", "value" : "upn"},
      { "name" : "SONARQUBE_OIDC_GROUP_SYNC", "value" : "True"},
      { "name" : "SONARQUBE_OIDC_GROUP_SYNC_CLAIM", "value" : "groups"},
      { "name" : "JAVA_OPTS", "value" : "-Dsonar.search.javaAdditionalOpts=-Dnode.store.allow_mmapfs=false"},
      { "name" : "SONARQUBE_LOG_CONSOLE", "value" : "true"}
    ],
    "portMappings": [
      {
        "containerPort": ${var.SONARQUBE.INTERNAL_PORT},
        "hostPort": ${var.SONARQUBE.EXTERNAL_PORT}
      }
    ]
  }
]
DEFINITION

}

resource "aws_ecs_task_definition" "nist_sync" {
  family                   = "nist-sync"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.NIST_SYNC.CPU_SHARES
  memory                   = var.NIST_SYNC.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.NIST_SYNC.CPU_SHARES},
    "image": "${var.NIST_SYNC_IMAGE}",
    "memory": ${var.NIST_SYNC.MEMORY},
    "name": "nist-sync",
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "nist-sync",
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "nist-sync"
        }
      },
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${var.NIST_SYNC.INTERNAL_PORT},
        "hostPort": ${var.NIST_SYNC.EXTERNAL_PORT}
      }
    ]
  }
]
DEFINITION

}

resource "aws_ecs_task_definition" "grafana" {
  family                   = "grafana"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.GRAFANA.CPU_SHARES
  memory                   = var.GRAFANA.MEMORY
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.grafana_role.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.GRAFANA.CPU_SHARES},
    "image": "${var.GRAFANA_IMAGE}",
    "memory": ${var.GRAFANA.MEMORY},
    "name": "grafana",
    "entryPoint": [
      "sh",
      "-c",
      "echo $GRAFANA_DATASOURCES_BASE64 | base64 -d > $GF_PATHS_PROVISIONING/datasources/datasources.yaml; echo $GRAFANA_DASHBOARDS_BASE64 | base64 -d > $GF_PATHS_PROVISIONING/dashboards/dashboards.yaml; mkdir -p /var/lib/grafana/dashboards; echo $GRAFANA_ECS_DASHBOARD_BASE64 | base64 -d > /var/lib/grafana/dashboards/ecs-container-insights.json; /run.sh"
    ],
    "environment": [
      { "name" : "GRAFANA_DATASOURCES_BASE64", "value" : "${base64encode(templatefile("${path.module}/grafana/grafana-datasources.yaml", local.grafana_vars))}" },
      { "name" : "GRAFANA_DASHBOARDS_BASE64", "value" : "${base64encode(templatefile("${path.module}/grafana/grafana-dashboard-providers.yaml", local.grafana_vars))}" },
      { "name" : "GRAFANA_ECS_DASHBOARD_BASE64", "value" : "${base64encode(templatefile("${path.module}/grafana/ecs-container-insights-dashboard.json", local.grafana_vars))}" },
      { "name" : "GF_SERVER_ROOT_URL", "value" : "https://grafana.${var.DNS_ZONE_NAME}" },
      { "name" : "GF_DATABASE_TYPE", "value" : "postgres" },
      { "name" : "GF_DATABASE_HOST", "value" : "${module.postgresql_grafana.db_instance_address[0]}" },
      { "name" : "GF_DATABASE_NAME", "value" : "${module.postgresql_grafana.db_instance_name[0]}" },
      { "name" : "GF_DATABASE_USER", "value" : "${module.postgresql_grafana.db_instance_username[0]}" },
      { "name" : "GF_DATABASE_PASSWORD", "value" : "${random_password.grafana_db_password.result}" },
      { "name" : "GF_SECURITY_ADMIN_PASSWORD", "value" : "${random_password.grafana_admin_password.result}" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_ENABLED", "value" : "true" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_CLIENT_ID", "value" : "${aws_cognito_user_pool_client.grafana_app_client.id}" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_CLIENT_SECRET", "value" : "${aws_cognito_user_pool_client.grafana_app_client.client_secret}" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_SCOPES", "value" : "openid profile email" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_AUTH_URL", "value" : "https://${aws_cognito_user_pool_domain.cognito_domain_name.domain}/oauth2/authorize" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_TOKEN_URL", "value" : "https://${aws_cognito_user_pool_domain.cognito_domain_name.domain}/oauth2/token" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_API_URL", "value" : "https://${aws_cognito_user_pool_domain.cognito_domain_name.domain}/oauth2/userInfo" },
      { "name" : "GF_AUTH_GENERIC_OAUTH_LOGIN_ATTRIBUTE_PATH", "value" : "email" }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-create-group": "true",
        "awslogs-group": "grafana",
        "awslogs-region": "${var.AWS_REGION}",
        "awslogs-stream-prefix": "grafana"
        }
      },
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${var.GRAFANA.INTERNAL_PORT},
        "hostPort": ${var.GRAFANA.EXTERNAL_PORT}
      }
    ]
  }
]
DEFINITION

}
