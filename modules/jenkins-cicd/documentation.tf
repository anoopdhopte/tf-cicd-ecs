locals {
  doc_vars = {
    region                      = var.AWS_REGION
    env_name                    = var.ENVIRONMENT
    account_name                = var.ACCOUNT_NAME
    project                     = var.PROJECT
    jenkins_slave_image         = element(split("/", var.JENKINS_SLAVE_IMAGE), 1)
    jenkins_master_image        = element(split("/", var.JENKINS_MASTER_IMAGE), 1)
    sonarqube_image             = element(split("/", var.SONARQUBE_IMAGE), 1)
    admin_users                 = join(", ", var.ADMIN_USERS_ID)
    jenkins_link                = format("%s.%s", "https://jenkins", var.DNS_ZONE_NAME)
    sonar_link                  = format("%s.%s", "https://sonar", var.DNS_ZONE_NAME)
    settings_xml                = templatefile("${path.module}/conf/settings.xml", local.settings_vars)
    dependency_check_properties = file("${path.module}/conf/dependency-check-properties")
  }
}

provider "confluence" {
  site = "idemiadigitallabs.atlassian.net"
}

resource "confluence_content" "default" {
  space  = "GID"
  title  = format("%s-%s-%s", var.PROJECT, var.ENVIRONMENT, var.ACCOUNT_NAME)
  body   = templatefile("${path.module}/documentation.html", local.doc_vars)
  parent = "2400485383"
}


