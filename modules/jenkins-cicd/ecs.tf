module "ecs_cluster" {
  source       = "git::https://bitbucket.org/anoopdhopte/ecs.git//modules/cluster?ref=1.0.7"
  globals      = module.common.globals
  CLUSTER_NAME = random_pet.ecs_cluster_name.id
}
