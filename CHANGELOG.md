# Change Log
-------------
## 1.4.0 2021-11-22
GID-2309: Add whitelist of IDs with admin users
GID-2259: Automated documentation
GID-1993: Monitoring of ECS containers

## 1.3.1 2021-08-25
Fix postgresql version

## 1.3.0 2021-08-25
GID-2016 Add Allure installation to CasC

## 1.2.0 2021-07-26
GID-1483 Allow to display build description as HTML

## 1.1.1 2021-07-20
GID-1825 - new security group a sign in to Docker Daemon

## 1.1.0 2021-07-14
GID-1825 - open security group for test in Docker

## 1.0.0 2021-07-09
Initial package migrated from previous repository

#### Added
- GID-37
- GID-60
- GID-230
- GID-231
- GID-232
- GID-233
- GID-236
- GID-361
- GID-538
- GID-542
- GID-543
- GID-547
- GID-633
- GID-742
- GID-750
- GID-770
- GID-772
- GID-774
- GID-942
- GID-1141
- GID-1163
- GID-1224
- GID-1234
- GID-1401
- GID-1602
- GID-1622
- GID-1686
- GID-1713
- GID-1727

